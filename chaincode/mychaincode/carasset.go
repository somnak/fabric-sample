package main

import (
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	sc "github.com/hyperledger/fabric/protos/peer"
)

type ChainCode struct {

}

type mycar struct {
	Maker string
	SerialNumber string
	ModelName string
	Color string
	Owner owner
}
type owner struct {
	Name string
	Age string
	Address string
}


func (cc *ChainCode) Init(stub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}

func (cc *ChainCode) Invoke(stub shim.ChaincodeStubInterface) sc.Response {
	function, args := stub.GetFunctionAndParameters()
	if(function =="initLedger"){
		return cc.initLedger(stub)
	}else if(function=="changeowner"){
		return cc.ChangeOwner(stub,args)
	}
	return shim.Error("Invalid Smart Contract function name.")
}
func (cc *ChainCode)initLedger(stub shim.ChaincodeStubInterface)  sc.Response{
	cars := []mycar{
		mycar{"TOYOTA","1F-123232","Ferari","Black",owner{"SomNak","12","pp"}},
		mycar{"TOYOTA","1F-124532","Ferari","Pink",owner{"SomNak","12","pp"}},
		mycar{"TOYOTA","1F-123632","Ferari","White",owner{"SomNak","12","pp"}},
		mycar{"TOYOTA","1F-123732","Ferari","Brown",owner{"SomNak","12","pp"}},
		mycar{"TOYOTA","1F-123832","Ferari","Black",owner{"SomNak","12","pp"}},
		mycar{"TOYOTA","1F-123232","Ferari","Black",owner{"SomNak","12","pp"}},
		mycar{"TOYOTA","1F-123432","Ferari","Black",owner{"SomNak","12","pp"}},
		mycar{"TOYOTA","1F-123932","Ferari","Black",owner{"SomNak","12","pp"}},
		mycar{"TOYOTA","1F-123132","Ferari","Black",owner{"SomNak","12","pp"}},
	}
	i := 0
	for i < len(cars) {
		fmt.Println("i is ", i)
		id:=cars[i].SerialNumber
		carAsBytes, _ := json.Marshal(cars[i])
		stub.PutState(id, carAsBytes)
		fmt.Println("Added", cars[i])
		i = i + 1
	}
	return shim.Success(nil)
}
func (cc *ChainCode) ChangeOwner(stub shim.ChaincodeStubInterface,args[] string) sc.Response {

	if len(args) != 4 {
		return shim.Error("Incorrect number of arguments. Expecting 2")
	}

	carAsBytes, _ := stub.GetState(args[0])
	car := mycar{}
	owner :=owner{args[1],args[2],args[3]}

	json.Unmarshal(carAsBytes, &car)
	car.Owner = owner

	carAsBytes, _ = json.Marshal(car)
	stub.PutState(args[0], carAsBytes)

	return shim.Success(nil)
}

func main()  {
	// Create a new Smart Contract
	err := shim.Start(new(ChainCode))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}